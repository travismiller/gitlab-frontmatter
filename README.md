# gitlab-frontmatter

GitLab Markdown display should render frontmatter in other formats besides just YAML.

The popular static site generator [Hugo supports YAML, TOML, and JSON](https://gohugo.io/content-management/front-matter/).

- [x]  YAML: [YAML.md](YAML.md)
- [ ]  TOML: [TOML.md](TOML.md)
- [ ]  JSON: [JSON.md](JSON.md), [JSON-no-token.md](JSON-no-token.md)
- [ ]  Arbitrary: [PHP.md](PHP.md)
